from django.shortcuts import render
from django.http import HttpResponse
from .models import Homepage


def index(request):
	homepages = Homepage.objects.all()
	return render(request, 'homepages/index.html', {'homepages' : homepages})


