from . import views
from django.contrib import admin
from django.urls import include,path

urlpatterns = [
	path('homepages/', include('homepages.urls')),
	path('',views.welcome),
    path('admin/', admin.site.urls),
]
